angular.module('MyApp', ['ngMaterial','ngAnimate', 'ui.bootstrap','firebase','ngRoute'])
.config(function($routeProvider){
  $routeProvider
    .when('/', {
      controller: 'AppCtrl',
      controllerAs: 'q',
      templateUrl: 'tmp/buscador.html'
    })
})
.directive('mbRepeatFin', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last) {
	  		      $timeout(function() { 
			      scope.$eval(attr.mbRepeatFin);
			   });
			}
        }
    }
})
.directive('mbRepeatIni', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$first) {
	  		      $timeout(function() { 
			      scope.$eval(attr.mbRepeatIni);
			   });
			}
        }
    }
})
.controller('AppCtrl', function($scope,$http) {
  	$scope.filtro={};
  	$scope.totalCursos=0;
  	$scope.visible=false;
  	$scope.loadingVisible=false;
  	$scope.cursos = [];
  	$scope.busqueda={title:''};
  	$scope.buscar=function(){
  		$scope.loadingVisible=true;
  		$scope.totalCursos=0;
  		if($scope.cursos.length==0){
  			$scope.cargarCursos();
  		}

  		$scope.filtro=$.extend({},$scope.busqueda);
  		$scope.visible=true;
  	}

  	$scope.cargarCursos=function(){
  		 $http.get("ojala.min.json")
    		.then(function(response) {
        		$scope.cursos = response.data;
        		// console.log(response.data);
    	});


  	}

  	$scope.cursoSeleccionado={};
  

  	$scope.seleccionarCurso=function(curso){
  		if($scope.cursoSeleccionado==curso){
  			$scope.cursoSeleccionado={};
  		}else{
  			$scope.cursoSeleccionado=curso;
  		}
  	}

  	$scope.sumarCursos=function(){
  		$scope.totalCursos++;

  	}

  	$scope.loading=function(sw){
  		if(sw=='ini'){
  			$scope.loadingVisible=true;
  		}else{
  			$scope.loadingVisible=false;
  		}
  	}
})
.controller('cip',function($scope,$http,$firebaseArray,firebaseRootRef){

//Defaults
  $scope.acceso={
    ip:null,
    fecha:new Date()
  }
  $scope.accesos=[];

  var rootRef=firebaseRootRef();

  //Capturar IP 
  $http.get('http://ipv4.myexternalip.com/json')
  .then(function(res){
    if(res.data.ip!='179.7.64.84'){
     $scope.acceso={
            ip:res.data.ip,
            fecha: new Date(new Date()+' UTC').toISOString().slice(0,19).replace('T',' ')
          }

      $http.get('http://ip-api.com/json/'+$scope.acceso.ip)
       .then(function(res2){
            $scope.acceso=angular.extend($scope.acceso,res2.data);
            $scope.accesos=$firebaseArray(rootRef);
            $scope.accesos.$add($scope.acceso);
      })
    }
   
  })

})
.controller('rip',function($scope, $http,$firebaseArray, firebaseRootRef){
     $scope.accesos=$firebaseArray(firebaseRootRef());

})
.factory('firebaseRootRef', function(){
  return function name(){
    var config = {
      apiKey: "AIzaSyAXF_TmmW_60ORFncXwOB7tyZkvZZAQknE",
      authDomain: "ng-ip-35496.firebaseapp.com",
      databaseURL: "https://ng-ip-35496.firebaseio.com",
      storageBucket: "ng-ip-35496.appspot.com",
    };
    firebase.initializeApp(config);

    var rootRef = firebase.database().ref();
    return rootRef;
  };
})
.filter("timeago", function () {
        //time: the time
        //local: compared to what time? default: now
        //raw: wheter you want in a format of "5 minutes ago", or "5 minutes"
        return function (time, local, raw,lang) {
            if (!time) return "never";

            if (!local) {
                (local = Date.now())
            }

            if (angular.isDate(time)) {
                time = time.getTime();
            } else if (typeof time === "string") {
                time = new Date(time).getTime();
            }

            if (angular.isDate(local)) {
                local = local.getTime();
            }else if (typeof local === "string") {
                local = new Date(local).getTime();
            }

            if (typeof time !== 'number' || typeof local !== 'number') {
                return;
            }

            var
                offset = Math.abs((local - time) / 1000),
                span = [],
                MINUTE = 60,
                HOUR = 3600,
                DAY = 86400,
                WEEK = 604800,
                MONTH = 2629744,
                YEAR = 31556926,
                DECADE = 315569260;

            var langs={
              es: {
                s: 'menos de un minuto',
               'm': 'minuto',
               'h':'hora',
               'd': 'día',
               'w':'semana',
               'y': 'año',
               'dec':'década',
               'more': 'Hace mucho tiempo',
               'ago': 'Hace'
              },
              en: {
                s: 'Less than a minute',
               'm': 'min',
               'h':'hr',
               'd': 'day',
               'w':'week',
               'y': 'year',
               'dec':'decade',
               'more': 'A long time',
               'ago': 'ago'
              }
            }

            if (offset <= MINUTE)              span = [ '', raw ? 'now' : 'less than a minute' ];
            else if (offset < (MINUTE * 60))   span = [ Math.round(Math.abs(offset / MINUTE)), 'min' ];
            else if (offset < (HOUR * 24))     span = [ Math.round(Math.abs(offset / HOUR)), 'hr' ];
            else if (offset < (DAY * 7))       span = [ Math.round(Math.abs(offset / DAY)), 'day' ];
            else if (offset < (WEEK * 52))     span = [ Math.round(Math.abs(offset / WEEK)), 'week' ];
            else if (offset < (YEAR * 10))     span = [ Math.round(Math.abs(offset / YEAR)), 'year' ];
            else if (offset < (DECADE * 100))  span = [ Math.round(Math.abs(offset / DECADE)), 'decade' ];
            else                               span = [ '', 'a long time' ];

            span[1] += (span[0] === 0 || span[0] > 1) ? 's' : '';
            span = span.join(' ');

            if (raw === true) {
                return span;
            }
            return (time <= local) ? span + ' ago' : 'in ' + span;
        }
    })

$(document).ready(function(){
	$('.oculto').removeClass('oculto');
})





