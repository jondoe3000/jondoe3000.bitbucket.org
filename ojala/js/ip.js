angular.module('miap',['firebase'])
.controller('cip',function($scope,$http,$firebaseArray){
	//Defaults
	$scope.acceso={
		ip:null,
		fecha:new Date()
	}
	$scope.accesos=[];

	//Config Firebase
	
	 var config = {
	    apiKey: "AIzaSyAXF_TmmW_60ORFncXwOB7tyZkvZZAQknE",
	    authDomain: "ng-ip-35496.firebaseapp.com",
	    databaseURL: "https://ng-ip-35496.firebaseio.com",
	    storageBucket: "ng-ip-35496.appspot.com",
	  };
	  firebase.initializeApp(config);

	  var rootRef = firebase.database().ref();

	//Capturar IP 
	$http.get('http://ipv4.myexternalip.com/json')
	.then(function(res){
		console.log('res.data',res.data);
		$scope.acceso={
			ip:res.data.ip,
			fecha: new Date(new Date()+' UTC').toISOString().slice(0,19).replace('T',' ')
		}

		$scope.accesos=$firebaseArray(rootRef);
		$scope.accesos.$add($scope.acceso);

		console.log('scope.accesos',$scope.accesos);
	})


	
})